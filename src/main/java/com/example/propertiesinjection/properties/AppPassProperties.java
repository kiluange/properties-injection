package com.example.propertiesinjection.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.HashMap;
import java.util.Map;

@ConfigurationProperties(prefix = "app")
@Getter
@Setter
public class AppPassProperties {
    Map<String, TestObject> config = new HashMap<>();
}
