package com.example.propertiesinjection.config;

import com.example.propertiesinjection.properties.AppPassProperties;
import com.example.propertiesinjection.properties.TestObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

@Configuration
@EnableConfigurationProperties(AppPassProperties.class)
@RequiredArgsConstructor(onConstructor = @__(@Lazy))
@Log4j2
public class TestConfig {
    private final AppPassProperties properties;

    @Bean
    public void thisIsATest() {
        for (TestObject testObject : this.properties.getConfig().values()) {
            log.info(testObject.toString());
        }
    }
}
